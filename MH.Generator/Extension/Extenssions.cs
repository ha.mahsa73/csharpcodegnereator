﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MH.Extensions;

namespace MH.Generator.Extension
{
    public static class Extenssions
    {
        public static string GetAssignFormat(this PropertyInfo property)
        {
            string str = property.Name.ToLowerCaseFirstChar();
            return string.Format("{0} = {1};", property.Name, str);
        }

        public static string GetAssignFormat(this PropertyInfo property, string leftParameterName, string rightParamerName)
        {
            string str = property.Name.ToLowerCaseFirstChar();
            return string.Format("{0}.{1} = {2}.{3};", new object[] { leftParameterName, property.Name, rightParamerName, property.Name });
        }

        public static string GetParameterFormat(this PropertyInfo property)
        {
            string str = property.Name.ToLowerCaseFirstChar();
            return string.Format("{0} {1},", property.GetPropertyTypeText(), str);
        }

        public static string GetPropertyText(this PropertyInfo property, bool isForDto = false)
        {
            StringBuilder builder = new StringBuilder();
         builder.AppendLine(string.Format("          public {0} {1} {{ get; set; }}", property.GetPropertyTypeText(), property.Name));
            return builder.ToString();
        }

        public static string GetPropertyTextForTypeScript(this PropertyInfo property)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format(" {0}:{1}", property.Name, property.GetPropertyTypeTextForTypeScript()));
            //builder.AppendLine(string.Format("        set {0}(value)  {1}         {{ this.{2} = value; }}", property.Name, Environment.NewLine, property.Name.ToLowerCaseFirstChar()));
            //builder.AppendLine(string.Format("        get {0}()  {1}         {{ return this.{2}; }}", property.Name, Environment.NewLine, property.Name.ToLowerCaseFirstChar()));
            return builder.ToString();
        }

        public static string GetPropertyTypeText(this PropertyInfo property)
        {
            if (property.PropertyType.IsNullable())
            {
                return string.Format("{0}?", property.PropertyType.GenericTypeArguments.First<Type>());
            }
            if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition().IsCollection())
            {
                return string.Format("List<{0}>", property.PropertyType.GenericTypeArguments.First<Type>());
            }
            return string.Format("{0}", property.PropertyType.Name);
        }

        public static string GetPropertyTypeTextForTypeScript(this PropertyInfo property)
        {
            if (property.PropertyType.IsNullable())
            {
                return string.Format("{0}", GetTypeScriptType(property.PropertyType.GenericTypeArguments.First<Type>()));
            }
            if (property.PropertyType.IsGenericType && property.PropertyType.GetGenericTypeDefinition().IsCollection())
            {
                return string.Format("Array<{0}>", GetTypeScriptType(property.PropertyType.GenericTypeArguments.First<Type>()));
            }
            return string.Format("{0}", GetTypeScriptType(property.PropertyType));
        }

        public static string GetTypeScriptType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                    return "boolean";

                case TypeCode.SByte:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.UInt16:
                case TypeCode.Int32:
                case TypeCode.UInt32:
                case TypeCode.Int64:
                case TypeCode.UInt64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.Decimal:
                    return "number";

                case TypeCode.DateTime:
                    return "Date";

                case TypeCode.String:
                    return "string";
            }
            if (type.Name.ToLower().Contains("guid"))
            {
                return "string";
            }
            return type.Name;
        }

        public static string RegexMultilineReplace(this string input, string pattern, string replacement)
        {
            return Regex.Replace(input, pattern, replacement, RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline | RegexOptions.Multiline);
        }

        public static string ToLowerCaseFirstChar(this string value)
        {
            return (char.ToLowerInvariant(value[0]) + value.Substring(1));
        }

        public static string ToUpperCaseFirstChar(this string value)
        {
            return (char.ToUpperInvariant(value[0]) + value.Substring(1));
        }
    }

}
