﻿using System;
using System.Collections.Generic;
using MH.Generator.Extension;

namespace MH.Generator.Base
{
    public abstract class ELementGeneratorBase : IELementGenerator
    {
        protected ELementGeneratorBase()
        {
        }

        public virtual string Generate(Type domainType, List<KeyValuePair<string, string>> constantValues, string inpute)
        {
            return inpute.RegexMultilineReplace(this.Element, constantValues.Find(c => c.Key == this.Key).Value);
        }

        public abstract string Element { get; }

        public abstract string Key { get; }
    }

}
