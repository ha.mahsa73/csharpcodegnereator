﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MH.Generator.Genrate;

namespace MH.Generator.Base
{
    public abstract class ClassGeneratorBase : IClassGenerator
    {
        public ClassGeneratorBase()
        {
            this.ELementGenerators = new List<IELementGenerator>();
            this.ELementGenerators.Add(new NameSpaceGenerator());
            this.ELementGenerators.Add(new ModelGenerator());
            this.ELementGenerators.Add(new IdTypeGenerator());
            this.ELementGenerators.Add(new TypeScriptModel());
            this.ELementGenerators.Add(new SubSystem());
        }

        public void Generate(System.Type domainType, List<KeyValuePair<string, string>> constantValues, string rootPath)
        {
            this.ModelName = constantValues.Find(c => c.Key == "Model").Value;
            this.NameSpaceName = constantValues.Find(c => c.Key == "NameSpace").Value;
            using (FileStream stream = new FileStream(this.TemplatePath, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string inpute = reader.ReadToEnd();
                    foreach (IELementGenerator generator in this.ELementGenerators)
                    {
                        inpute = generator.Generate(domainType, constantValues, inpute);
                    }
                    IOHelpers.WriteDataToFiles(Path.Combine(rootPath, this.GetSavePath()),
                        new KeyValuePair<string, string>(this.FileName, inpute));
                }
            }
        }

        public string GetSavePath()
        {
            if (this.SavePathConntainsModelName)
            {
                return string.Format(@"{0}\{1}", this.SavedPath, this.ModelName);
            }
            return this.SavedPath;
        }

        public string ApplicationPath
        {
            get { return Path.GetDirectoryName(Application.ExecutablePath); }
        }

        public List<IELementGenerator> ELementGenerators { get; set; }

        public abstract string FileName { get; }

        public string ModelName { get; set; }

        public string NameSpaceName { get; set; }

        public abstract string SavedPath { get; }

        public virtual bool SavePathConntainsModelName
        {
            get { return true; }
        }

        public abstract string TemplatePath { get; }
    }
}
