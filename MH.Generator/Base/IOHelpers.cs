﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MH.Generator.Base
{
    public static class IOHelpers
    {
        public static string CreateFolderIfAbsence(string absoluteFolderPath)
        {
            if (!Directory.Exists(absoluteFolderPath))
            {
                Directory.CreateDirectory(absoluteFolderPath);
            }
            return absoluteFolderPath;
        }

        public static void WriteDataToFiles(string folderPath, KeyValuePair<string, string> filenameAndContent)
        {
            CreateFolderIfAbsence(folderPath);
            using (StreamWriter writer = new StreamWriter(Path.Combine(folderPath, filenameAndContent.Key), false, Encoding.UTF8))
            {
                Application.DoEvents();
                writer.WriteLine(filenameAndContent.Value);
                writer.Flush();
                writer.Close();
                writer.Dispose();
            }
        }

        public class Folders
        {
            public Folders(string source, string target)
            {
                this.Source = source;
                this.Target = target;
            }

            public string Source { get; private set; }

            public string Target { get; private set; }
        }
    }

}
