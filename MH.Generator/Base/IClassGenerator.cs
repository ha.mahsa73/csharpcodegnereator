﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH.Generator
{
    public interface IClassGenerator
    {
        // Methods
        void Generate(Type domainType, List<KeyValuePair<string, string>> constantValues, string rootPath);

        // Properties
        List<IELementGenerator> ELementGenerators { get; set; }
        string SavedPath { get; }
        string TemplatePath { get; }
    }





}
