﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH.Generator
{
    public interface IELementGenerator
    {
        // Methods
        string Generate(Type domainType, List<KeyValuePair<string, string>> constantValues, string inpute);

        // Properties
        string Element { get; }
        string Key { get; }
    }



}
