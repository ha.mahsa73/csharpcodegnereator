﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;
using MH.Generator.Extension;

namespace MH.Generator.Genrate
{
    public class UIComponnetGenerator : ClassGeneratorBase
    {
        public UIComponnetGenerator()
        {
            base.ELementGenerators.Add(new UIDtoModelPropertiesGenerator());
        }

        public override string FileName
        {
            get
            {
                return string.Format("{0}.ts", base.ModelName+"Component".ToLowerCaseFirstChar());
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"UI\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\UI\Component.txt");
            }
        }
    }

}
