﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class RepositoryGenerator : ClassGeneratorBase
    {
        public override string FileName
        {
            get
            {
                return string.Format("{0}Repository.cs", base.ModelName);
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"Persistance\Repository\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\Infrastructure\Repository.txt");
            }
        }
    }
}
