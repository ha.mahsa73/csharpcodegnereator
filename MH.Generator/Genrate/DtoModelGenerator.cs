﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class DtoModelGenerator : ClassGeneratorBase
    {
        public DtoModelGenerator()
        {
            base.ELementGenerators.Add(new DtoModelPropertiesGenerator());
        }

        public override string FileName
        {
            get
            {
                return string.Format("{0}Dto.cs", base.ModelName);
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"DataContract\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\DataContract\DtoModel.txt");
            }
        }
    }
}
