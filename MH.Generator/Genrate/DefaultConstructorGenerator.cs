﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class DefaultConstructorGenerator : ClassGeneratorBase
    {
        public DefaultConstructorGenerator()
        {
            base.ELementGenerators.Add(new PropertiesWithTypesGenerator());
            base.ELementGenerators.Add(new PropertyAssignmentInConstructorGenerator());
        }

        public override string FileName
        {
            get
            {
                return string.Format("{0}Constructor.txt", base.ModelName);
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"DomainModel\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\Domain\DefaultConstructor.txt");
            }
        }
    }
}
