﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class DomainServiceInterfaceGenerator : ClassGeneratorBase
    {
        public override string FileName
        {
            get
            {
                return string.Format("I{0}Service.cs", base.ModelName);
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"ServiceInterface\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\Domain\Service.Core\DomainServiceInterface.txt");
            }
        }
    }
}
