﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class ModelGenerator : ELementGeneratorBase
    {
        public override string Element
        {
            get
            {
                return @"\{Model\}";
            }
        }

        public override string Key
        {
            get
            {
                return "Model";
            }
        }
    }

}
