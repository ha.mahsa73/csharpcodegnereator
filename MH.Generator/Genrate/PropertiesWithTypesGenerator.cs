﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;
using MH.Generator.Extension;

namespace MH.Generator.Genrate
{
    public class PropertiesWithTypesGenerator : ELementGeneratorBase
    {
        public override string Generate(Type domainType, List<KeyValuePair<string, string>> constantValues, string inpute)
        {
            StringBuilder builder = new StringBuilder();
            foreach (PropertyInfo info in domainType.GetProperties())
            {
                if (info.Name.ToLower() != "events")
                {
                    builder.AppendLine(info.GetParameterFormat());
                }
            }
            string replacement = builder.ToString();
            replacement = replacement.Remove(replacement.LastIndexOf(','));
            return inpute.RegexMultilineReplace(this.Element, replacement);
        }

        public override string Element
        {
            get
            {
                return @"\{PropertiesWithTypes\}";
            }
        }

        public override string Key
        {
            get
            {
                return "PropertiesWithTypes";
            }
        }
    }
}
