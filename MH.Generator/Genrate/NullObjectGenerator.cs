﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH.Generator.Base;

namespace MH.Generator.Genrate
{
    public class NullObjectGenerator : ClassGeneratorBase
    {
        public override string FileName
        {
            get
            {
                return string.Format("Null{0}.cs", base.ModelName);
            }
        }

        public override string SavedPath
        {
            get
            {
                return @"DomainModel\NullObjects\";
            }
        }

        public override string TemplatePath
        {
            get
            {
                return string.Format(@"{0}\{1}", base.ApplicationPath, @"Resources\Domain\NullObject.txt");
            }
        }
    }
}
