﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MH.Generator.Extension;
using MH.Generator.Genrate;

namespace MH.Generator
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        private List<IClassGenerator> generators;




        private void Form1_Load(object sender, EventArgs e)
        {



        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "dll files (*.dll)|*.dll|All files (*.*)|*.*"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.AssemblyPathTextBox.Text = dialog.FileName;
            }


        }

        private void ConfigureGeneratorItems()
        {
            this.generators = new List<IClassGenerator>();
            this.generators.Add(new DomainServiceGenerator());
            this.generators.Add(new DomainServiceInterfaceGenerator());
            this.generators.Add(new NullObjectGenerator());
            this.generators.Add(new RepositoryInterfaceGenerator());
            this.generators.Add(new RepositoryGenerator());
            this.generators.Add(new ApplicationServiceGenerator());
            this.generators.Add(new IocRegistryGenerator());
            this.generators.Add(new EntityMapperGenerator());
            this.generators.Add(new DefaultConstructorGenerator());
            this.generators.Add(new DtoModelGenerator());
            this.generators.Add(new ApplicationServiceContractGenerator());
            this.generators.Add(new UIModelGenerator());
            this.generators.Add(new UIComponnetGenerator());
            this.generators.Add(new UIServiceGenerator());


        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            this.ConfigureGeneratorItems();
            List<KeyValuePair<string, string>> constantValues = new List<KeyValuePair<string, string>>();
            Assembly assembly = Assembly.LoadFrom(this.AssemblyPathTextBox.Text);
            Type domainType = assembly.GetType(string.Format("{0}.{1}", assembly.ManifestModule.Name.Replace(".dll", ""), this.DomainTypeTextBox.Text));
            if (domainType != null)
            {
                string str = string.Format("{0}.{1}", this.SolutionTextBox.Text, this.SubSystemTextBox.Text);
                string str2 = this.SolutionTextBox.Text.ToLower();
                string str3 = this.SubSystemTextBox.Text.ToLowerCaseFirstChar();
                string str4 = string.Format("{0}.{1}", str2, str3);
                constantValues.Add(new KeyValuePair<string, string>("Solution", this.SolutionTextBox.Text));
                constantValues.Add(new KeyValuePair<string, string>("SubSystem", this.SubSystemTextBox.Text));
                constantValues.Add(new KeyValuePair<string, string>("NameSpace", str));
                constantValues.Add(new KeyValuePair<string, string>("Model", this.DomainTypeTextBox.Text));
                constantValues.Add(new KeyValuePair<string, string>("TypeScriptSolution", str2));
                constantValues.Add(new KeyValuePair<string, string>("TypeScriptSubSystem", str3));
                constantValues.Add(new KeyValuePair<string, string>("TypeScriptAggrigate", this.aggrigateText.Text));
                constantValues.Add(new KeyValuePair<string, string>("TypeScriptNameSpace", str3+this.aggrigateText.Text));
                constantValues.Add(new KeyValuePair<string, string>("TypeScriptModel", this.DomainTypeTextBox.Text));
                constantValues.Add(new KeyValuePair<string, string>("IdType", domainType.GetProperty("Id").PropertyType.Name));
                foreach (IClassGenerator generator in this.generators)
                {
                    generator.Generate(domainType, constantValues, this.GeneratedPathTextBox.Text);
                }
                MessageBox.Show("Operation is completed.");
                Process.Start(this.GeneratedPathTextBox.Text);
            }
            else
            {
                MessageBox.Show(string.Format("'{0}' assembly does not contains '{1}' type.", assembly.ManifestModule.Name, this.DomainTypeTextBox.Text));
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
          
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.GeneratedPathTextBox.Text = folderBrowserDialog1.SelectedPath;
            
            }

        }
    }

   
}
