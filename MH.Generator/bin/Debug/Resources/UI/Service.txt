import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as globals from '../../../../../helpers/globals';
import 'rxjs/add/operator/map';
import { Authentication } from '../../../../login/Authentication';
@Injectable()
export class {TypeScriptNameSpace}{TypeScriptModel}Service {
  token: string;

  constructor(private http: Http) {
    let currentUser = null;
    if (localStorage.getItem('Authentication') !== 'undefined') {
      currentUser = JSON.parse((localStorage.getItem('Authentication'))) as Authentication;
      this.token = 'Bearer ' + currentUser.accessTokenHash;
    }
  }

  Get(model: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(model);
    return this.http.get(globals.{TypeScriptSubSystem}Api+'{TypeScriptModel}/FindAll',options).map(res => res.json());
  }

  Create(model: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':this.token,'Access-Control-Allow-Origin':'*' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(model);
    return this.http.post(globals.{TypeScriptSubSystem}Api +'{TypeScriptModel}/Save', body, options).map(res => res.json());
  }

  Remove (model: any): Observable<any[]> {
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':this.token,'Access-Control-Allow-Origin':'*' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(model);
    return this.http.post(globals.{TypeScriptSubSystem}Api +'{TypeScriptModel}/Delete', body, options).map(res => res.json());
  }
}
